/**********************************************************************
 * file:  sr_router.c
 * date:  Mon Feb 18 12:50:42 PST 2002
 * Contact: casado@stanford.edu
 *
 * Description:
 *
 * This file contains all the functions that interact directly
 * with the routing table, as well as the main entry method
 * for routing.
 *
 **********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>

#include "sr_if.h"
#include "sr_rt.h"
#include "sr_router.h"
#include "sr_protocol.h"
#include "sr_arpcache.h"
#include "sr_utils.h"

/* TODO: Add constant definitions here... */
const int ETHERNET_LENGTH  = 6;
const int IP_LENGTH = 4;
/* TODO: Add helper functions here... */

/* See pseudo-code in sr_arpcache.h */
void handle_arpreq(struct sr_instance* sr, struct sr_arpreq *req){
  /* TODO: Fill this in */
  printf("Inside handle_arpreq\n");
}

/*---------------------------------------------------------------------
 * Method: sr_init(void)
 * Scope:  Global
 *
 * Initialize the routing subsystem
 *
 *---------------------------------------------------------------------*/

void sr_init(struct sr_instance* sr)
{
    /* REQUIRES */
    assert(sr);

    /* Initialize cache and cache cleanup thread */
    sr_arpcache_init(&(sr->cache));

    pthread_attr_init(&(sr->attr));
    pthread_attr_setdetachstate(&(sr->attr), PTHREAD_CREATE_JOINABLE);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_t thread;

    pthread_create(&thread, &(sr->attr), sr_arpcache_timeout, sr);

    /* TODO: (opt) Add initialization code here */
   /* printf("  ---------   \n");
    char* itName = "eth3";
    sr_if* srIf = sr_get_interface(sr,itName);
    printf("  ---------   \n");
	*/
} /* -- sr_init -- */

/*---------------------------------------------------------------------
 * Method: sr_handlepacket(uint8_t* p,char* interface)
 * Scope:  Global
 *
 * This method is called each time the router receives a packet on the
 * interface.  The packet buffer, the packet length and the receiving
 * interface are passed in as parameters. The packet is complete with
 * ethernet headers.
 *
 * Note: Both the packet buffer and the character's memory are handled
 * by sr_vns_comm.c that means do NOT free either (signified by "lent" comment).
 * Make a copy of the
 * packet instead if you intend to keep it around beyond the scope of
 * the method call.
 *
 *---------------------------------------------------------------------*/

void sr_handlepacket(struct sr_instance* sr,
        uint8_t * packet/* lent */,
        unsigned int len,
        char* interface/* lent */){

  /* REQUIRES */
  assert(sr);
  assert(packet);
  assert(interface);
  
  printf("*** <- sr_handlepacket got a packet of length %d",len);
  printf(", from interface: %s -> ***\n",interface);

  /* TODO: Add forwarding logic here */

  /*print_hdrs(packet, len);
  return;*/

  /* Type cast the buf pointer */
   int minlength = sizeof(sr_ethernet_hdr_t);
  /* Minimum Length of Ethernet Packet */
  if (len < minlength) {
    fprintf(stderr, "Failed to process ETHERNET header, insufficient length\n");
    return;
  }

  uint16_t ethtype = ethertype(packet);
  /*print_hdr_eth(packet);*/

  /* If ARP: */
  if (ethtype == ethertype_arp) {
  	printf("Ethtype == ethertype_arp\n");
    minlength += sizeof(sr_arp_hdr_t);
    if (len < minlength){
      fprintf(stderr, "Failed to process ARP header, insufficient length\n");
    }
    else{
      	sr_arp_hdr_t *arp_hdr = (sr_arp_hdr_t *)(packet + sizeof(sr_ethernet_hdr_t));
    	/* arp request */
    	if(ntohs(arp_hdr->ar_op)==1){
    		printf("arp request\n");
    		struct sr_if* itf = sr_get_interface(sr,interface);
    		assert(itf);
    		if((arp_hdr->ar_tip) == (itf->ip)){
				printf("The arp requests target ip is the router!!\n");
				uint8_t* arp = (uint8_t *) malloc(sizeof(sr_ethernet_hdr_t) + sizeof(sr_arp_hdr_t));   
				sr_ethernet_hdr_t* ethernetHeader = (sr_ethernet_hdr_t*) arp;
				sr_arp_hdr_t* arp_header = (sr_arp_hdr_t*) (arp + sizeof(sr_ethernet_hdr_t));
				assert(arp);
				memcpy(ethernetHeader->ether_dhost, arp_hdr->ar_sha, ETHERNET_LENGTH);
				memcpy(ethernetHeader->ether_shost, itf->addr, ETHERNET_LENGTH);
				ethernetHeader->ether_type = htons(ethertype_arp);
				arp_header->ar_hrd = htons(arp_hrd_ethernet); 
				arp_header->ar_pro = htons(ethertype_ip);
				arp_header->ar_hln = ETHERNET_LENGTH;
				arp_header->ar_pln = IP_LENGTH;
				arp_header->ar_op = htons(arp_op_reply);
				memcpy(arp_header->ar_sha, itf->addr, ETHERNET_LENGTH);
				arp_header->ar_sip = itf->ip;
				memcpy(arp_header->ar_tha, arp_hdr->ar_sha, ETHERNET_LENGTH);
				arp_header->ar_tip = arp_hdr->ar_sip;
				printf("Send an arp reply with router MAC now...\n");
				sr_send_packet(sr, arp, sizeof(sr_ethernet_hdr_t)+sizeof(sr_arp_hdr_t), interface);   
				/*print_hdrs((uint8_t *)arp,sizeof(sr_ethernet_hdr_t)+sizeof(sr_arp_hdr_t));*/
				free(arp);

			   }
  			else {
  				printf("ARP request but not the router:\n");
          handle_arpreq(sr, (struct sr_arpreq *)packet); 
  			}
    	}
    	/* arp reply */
    	else if(ntohs(arp_hdr->ar_op)==2){
    		printf("ARP reply\n");
    	}
    	/* other ARP opcode, discard */
    	else{
    		printf("unknow arp opcode, dicard arp pakcet\n");
    	}
    }
  }
  /* If IP:  */
  else if (ethtype == ethertype_ip) {
	printf("Ethtype == ethertype_ip\n");
    minlength += sizeof(sr_ip_hdr_t);
    if (len < minlength) {
      fprintf(stderr, "Failed to process IP header, insufficient length\n");
      return;
    }
    uint8_t ip_proto = ip_protocol(packet + sizeof(sr_ethernet_hdr_t));
    if(ip_proto == ip_protocol_icmp){

      minlength += sizeof(sr_icmp_hdr_t);
      if (len < minlength){
        fprintf(stderr, "Failed to process ICMP header, insufficient length\n");
        return;
      }
      printf(" -- ICMP packet --\n");
      sr_ip_hdr_t *iphdr = (sr_ip_hdr_t *)(packet + sizeof(sr_ethernet_hdr_t));
      sr_icmp_hdr_t *icmp_hdr = (sr_icmp_hdr_t *)(packet + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t));
      printf(" IP Header cksum = %d\n",iphdr->ip_sum);
      int ipHdrLength = sizeof(sr_ip_hdr_t);
      int icmpHdrLength = sizeof(sr_icmp_hdr_t);
      uint16_t ipcksum = cksum (iphdr,ipHdrLength);
      uint16_t icmpcksum = cksum(icmp_hdr,icmpHdrLength);

      /*
      printf("ipHdrLength = %d\n",ipHdrLength);
      printf("icmpHdrLength = %d\n",icmpHdrLength);
      printf("ip_hl*4 = %d\n", (iphdr->ip_hl)*4);
      */

      
      printf(" IP cksum computed = : %d \n",ipcksum);
      printf(" ICMP Header cksum = %d\n",icmp_hdr->icmp_sum);
      printf(" ICMP cksum computed = : %d \n",icmpcksum);
	
    }
    else{
    	printf(" -- regular IP packet --\n");
    }
  }
  else {
    fprintf(stderr, "Unrecognized Ethernet Type: %d \n", ethtype);
  }
 printf("-------------------    done handling packet   ----------------\n");


}/* -- sr_handlepacket -- */